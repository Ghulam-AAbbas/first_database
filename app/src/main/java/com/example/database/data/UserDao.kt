package com.example.database.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.database.model.User
import java.util.concurrent.Flow


@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUser(user: User)

    @Update
    suspend fun updateUser(user: User)

    @Delete
    suspend fun deleteUser(user: User)

    @Query("SELECT * FROM user_table WHERE firstName LIKE :searchQuery OR lastName LIKE :searchQuery")
    fun searchUser(searchQuery: String): LiveData<List<User>>

//    @Query("SELECT id FROM user_table")
//    suspend fun deleteAllUser()
//
//    @Query("SELECT FROM user_table")
//    suspend fun deleteAllUser()

    @Query("SELECT * FROM user_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<User>>
}