package com.example.database.repository

import android.app.DownloadManager
import androidx.lifecycle.LiveData
import com.example.database.data.UserDao
import com.example.database.model.User

class UserRepository(private val userDao: UserDao) {

    val readAllData: LiveData<List<User>> = userDao.readAllData()

    suspend fun addUser(user: User){
        userDao.addUser(user)
    }

    suspend fun updateUser(user: User){
        userDao.updateUser(user)
    }

    suspend fun deleteUser(user: User){
        userDao.deleteUser(user)
    }

//    suspend fun deleteAllUser(){
//        userDao.deleteAllUser()
//    }

    fun searchUser(searchQuery: String): LiveData<List<User>> {
       return userDao.searchUser(searchQuery)
    }

}