package com.example.database

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Query
import com.example.database.viewModel.UserViewModel
import com.example.database.fragments.list.ListAdapter
import kotlinx.android.synthetic.main.fragment_list.view.*


class ListFragment : Fragment(), SearchView.OnQueryTextListener {

    private lateinit var mUserViewModel: UserViewModel
    private val listAdapter: ListAdapter by lazy { ListAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        // RecyclerView
        val adapter = ListAdapter()
        val recyclerView = view.recyclerview
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // User ViewModel
        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        mUserViewModel.readAllData.observe(viewLifecycleOwner, Observer {
            user ->
            adapter.setData(user)
        })

        view.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_listFragment_to_addFragment)
        }

        setHasOptionsMenu(true)

        return view
    }

    // Search View

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        val search = menu?.findItem(R.id.menu_search)
        val searchView = search?.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (query != null){
            searchUser(query)
        }
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        if (query != null){
            searchUser(query)
        }
        return true
    }

    private fun searchUser(query: String){
        val searchQuery = "%$query%"

        mUserViewModel.searchUser(searchQuery).observe(this, { list ->
            list.let {
                listAdapter.setData(it)
            }

        })
    }


//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        if (item.itemId == R.id.menu_delete){
//            deleteAllUser()
//        }
//        return super.onOptionsItemSelected(item)
//    }
//
//    private fun deleteAllUser() {
//        val builder = AlertDialog.Builder(requireContext())
//        builder.setPositiveButton("Yes"){_,_->
//            mUserViewModel.deleteAllUser()
//            Toast.makeText(requireContext(), "Successfully removed everything", Toast.LENGTH_LONG).show()
//        }
//        builder.setNegativeButton("No"){_,_->}
//        builder.setTitle("Delete Everything")
//        builder.setMessage("Are you sure to want delete Everything?")
//        builder.create().show()
//    }
}